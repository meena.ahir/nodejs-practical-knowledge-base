const express = require('express');
const contentController = require('./contentController');
const multipart = require('connect-multiparty');
const multipartMiddleware = multipart();

const contentRoute = express.Router();
// Get content by category
const getCategoriesContent = [contentController.getCategoriesContent];
contentRoute.get('/:categoryId', getCategoriesContent);
// Add content
const addCategoryContent = [contentController.addCategoryContent];
contentRoute.post('/', multipartMiddleware, addCategoryContent);

module.exports = contentRoute;