const contentModel = require('./contentModel');
const aws = require('../../helper/aws');

const contentController = {};

contentController.getCategoriesContent = async(req, res) => {
    const {
        categoryId
    } = req.params;
    const contentData = await contentModel.find({categoryId});
    res.status(200).json({
        contentData,
        message: 'Success'
    });
};

contentController.addCategoryContent = async(req, res) => {
    const {
        categoryId,
        title,
        description
    } = req.body;

    let insertData = {
        categoryId,
        title,
        description
    };

    const {
        image
    } = req.files;
    
    if (image) {
        const uploadedImg = await aws.uploadImageToS3(image, "original");
        insertData = { ...insertData, image: uploadedImg };
    }

    let content = await new contentModel(insertData);
    
    await content.save().then((contentData) => {
        res.status(200).json({
            contentData,
            message: 'Content added'
        });
    });
};

module.exports = contentController;