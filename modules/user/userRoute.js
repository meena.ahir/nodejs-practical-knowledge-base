const express = require('express');
const userController = require('./userController');
const middelware = require('../../helper/middleware');
const userRoute = express.Router();

// Signup API
userMiddleware = [middelware.checkEmailIdExistsOrNot, userController.signup];
userRoute.post('/sign-up', userMiddleware);
// Login API
userRoute.post('/login', userController.login);

module.exports = userRoute;