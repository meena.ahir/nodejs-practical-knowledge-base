const userModel = require("./userModel");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const validator = require("../../helper/validator");
const userController = {};

userController.signup = async (req, res) => {
  const { errors, isValid } = validator.checkValidation(req.body);
    if (!isValid) {
    res.status(404).json(errors);
    }
    req.body.email = req.body.email.toLowerCase();
    const {
      email,
      firstName,
      lastName,
      password,
      socialPlatform
    } = req.body;
    userModel.findOne({ email }).then((user) => {
      if (user) {
        res.status(409).json("email is already registered.");
      } else {
        const registerNewUser = new userModel({
          firstName,
          lastName,
          email
        });
        if (!socialPlatform) {
          bcrypt.genSalt(10, (err, salt) => {
            bcrypt.hash(password, salt, (err, hash) => {
              if (err) throw err;
              registerNewUser.password = hash;
              registerNewUser.save().then((user) => {
                res.json(user);
              }).catch((err) => console.log(err));
            });
          });
        } else {
          registerNewUser.socialPlatform = socialPlatform;
          registerNewUser.save().then((user) => {
            res.json(user);
          }).catch((err) => console.log(err));
        }
      }
    });
};

userController.login = async (req, res) => {
  const { errors, isValid } = validator.checkValidation(req.body);
    if (!isValid) {
      res.status(404).json(errors);
    } else {
      const {
        email,
        password
      } = req.body;
      userModel.findOne({ email }).then((user) => {
        if (!user) {
          res.status(404).json({ email: "email id does not exist" });
        } else {
          bcrypt.compare(password, user.password).then((isMatch) => {
            if (!isMatch) {
              res.status(400).json({ error: "Password do not match" });
            } else {
              const payLoad = {
                id: user.id,
              };
              jwt.sign(
                payLoad,
                process.env.JWT_SECRET,
                { expiresIn: 215526 },
                (err, token) => {
                  res.json({
                  success: true,
                  token: token,
                  userData: user,
                  });
                }
              );
            }
          });
        }
      });
    }
};

module.exports = userController;
