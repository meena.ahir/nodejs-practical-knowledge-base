const categoryModel = require('./categoryModel');

const categoryController = {};

categoryController.getCategories = async(req, res) => {
    req.body.userId = req.userData._id;
    const {
        userId
    } = req.body;
    const categoryData = await categoryModel.find({userId});
    res.status(200).json({
        categoryData: categoryData,
        totalCategories: categoryData.length,
        message: 'Success'
    });
};

categoryController.addCategory = async(req, res) => {
    req.body.userId = req.userData._id;
    const {
        userId,
        title,
        description
    } = req.body;

    let categories = await new categoryModel({
        userId,
        title,
        description
    });
    
    await categories.save().then((categories) => {
        res.status(200).json({
            categoryData: categories,
            message: 'Category created'
        });
    });
};

categoryController.updateCategory = async(req, res) => {
    req.body.userId = req.userData._id;
    const {
        userId,
        title,
        description
    } = req.body;

    const {
        id
    } = req.params;
    
    const updateCategory = await categoryModel.findOneAndUpdate(
        {_id: id, userId},
        {$set: {title, description}}
    );
    if (updateCategory) {
        res.status(200).json({
            message: 'Category Updated'
        });
    } else {
        res.status(401).json({message: 'Not Valid Request'});
    }
};

categoryController.deleteCategory = async(req, res) => {
    const {
        id
    } = req.params;
    const deleteCategory = await categoryModel.deleteOne({_id: id});
    if (deleteCategory) {
        res.status(200).json({
            message: 'Category Deleted'
        });
    } else {
        res.status(401).json({message: 'Not Valid Request'});
    }
};

module.exports = categoryController;