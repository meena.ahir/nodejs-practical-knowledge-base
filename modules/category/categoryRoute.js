const express = require('express');
const categoryController = require('./categoryController');
const categoryRoute = express.Router();

// List categories
const getCategories = [categoryController.getCategories];
categoryRoute.get('/', getCategories);
// Create category
const addCategory = [categoryController.addCategory];
categoryRoute.post('/', addCategory);
// Update category
const updateCategory = [categoryController.updateCategory];
categoryRoute.put('/:id', updateCategory);
// Delete category
const deleteCategory = [categoryController.deleteCategory];
categoryRoute.delete('/:id', deleteCategory);

module.exports = categoryRoute;