const jwt = require('jsonwebtoken');
const data = {};

data.getCurrentUserId = async (data) => {
    var token = data;
    var userID = "";
    if (token) {
        var decoded = await jwt.decode(token, process.env.JWT_SECRET);
        userID = decoded.id;
    }
    return userID;
}

module.exports = data;