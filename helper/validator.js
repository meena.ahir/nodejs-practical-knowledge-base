const mongoose = require('mongoose');
const validator = require('validator');
const isEmpty = require('is-empty');

module.exports.checkValidation = (data) => {
    const errors = {};
    data.email = !(isEmpty(data.email)) ? data.email : '';
    data.password = !(isEmpty(data.password)) ? data.password : '';

    if (validator.isEmpty(data.email)) {
        errors.email = 'Email is required';
    }
    if (validator.isEmpty(data.password)) {
        errors.email = 'Password is required';
    }
    if (!validator.isEmail(data.email)) {
        errors.email = 'Please provide valid email';
    }
    return {
        errors: errors,
        isValid: isEmpty(errors)
    }
}
