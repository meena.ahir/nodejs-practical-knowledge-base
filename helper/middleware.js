const middleware = {};
const userModel = require('../modules/user/userModel');
const getUserId = require('./getUserId');

middleware.checkEmailIdExistsOrNot = (req, res, next) => {
    const {email} = req.body;
    userModel.findOne({email: email.toLowerCase()}).then((user) => {
        if (user) {
            return res.status(400).json({
                message: 'Email is already registered.'
            });
        } else {
            next();
        }
    }).catch((error) => {
        return res.status(400).json({
            message: error
        });
    });
};

middleware.checkUserIsLoggedInOrNot = async (req, res, next) => {
    let token = (req.headers && req.headers['x-auth-token']);
    let userId = await getUserId.getCurrentUserId(token);
    if (!userId) {
        res.status(401).json({ "message": "NOT_AUTHORIZED" });
    }
    else {
        userModel.findOne({ "_id": userId }).exec(function (err, user) {
            if (!user) {
                res.status(401).json({ "message": "NOT_AUTHORIZED" });
                return;
            } else {
                req.userData = user;
                next();
            }
        })
    }
}

module.exports = middleware;